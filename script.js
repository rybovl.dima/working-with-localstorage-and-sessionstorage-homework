document.addEventListener('DOMContentLoaded', () => {
    const switchBtn = document.getElementById('switch-btn');
    const body = document.body;
    
    if (localStorage.getItem('theme') === 'alternate') {
        body.classList.add('alternate-theme');
    } else {
        body.classList.add('default-theme');
    }

    switchBtn.addEventListener('click', () => {
        if (body.classList.contains('default-theme')) {
            body.classList.replace('default-theme', 'alternate-theme');
            localStorage.setItem('theme', 'alternate');
        } else {
            body.classList.replace('alternate-theme', 'default-theme');
            localStorage.setItem('theme', 'default');
        }
    });
});